<!DOCTYPE html>
<?php
    require_once __DIR__ . '/facebook-sdk-v5/autoload.php';
    date_default_timezone_set('UTC');
    use Facebook\FacebookSession;
    use  Facebook\FacebookClient;
    use Facebook\FacebookRequest; 
?>

<html>
    <head>
        <title>Facebook Search</title>
        <meta charset="UTF-8">
    <script type="text/javascript">
        function onTypeSelection()
        {
            var elementToHide = document.getElementById("location_field");
            
            var selectElement = document.getElementById("selectField");
            if(selectElement.value == "user"){
                elementToHide.style.display = "none"; 
            }
            else if(selectElement.value == "page"){
                elementToHide.style.display = "none";
            }
            else if(selectElement.value == "event"){
                elementToHide.style.display = "none"; 
            }
            else if(selectElement.value == "place"){
                elementToHide.style.display = "table-row";
            }
            else if(selectElement.value == "group"){
                elementToHide.style.display = "none"; 
            } 
        }
        
        function emptyFields()
        {
            document.getElementById("textAreaKeyword").value = "";
            document.getElementById("textAreaLocation").value = "";
            document.getElementById("textAreaDistance").value = "";
            var elementToHide = document.getElementById("location_field");
            var selectElement = document.getElementById("selectField");
            selectElement.value = "user";
            elementToHide.style.display = "none"; 
            document.getElementById("textAreaLocation").required = false;
            document.getElementById("textAreaDistance").required = false; 
            
            var php = document.getElementById("result");       
            if(php)
            {
                    php.parentNode.removeChild(php);
            }
            
            var details = document.getElementById("detailsPHP")
            if(details)
            {
                    details.parentNode.removeChild(details);
            }
        }
        
        function hide(item)
        {
            var hidePost = document.getElementById(item);
            if(hidePost)
            {
                if(hidePost.style.display == 'none')
                    {
                        hidePost.style.display = 'block';
                        if(item == "resultAlbums"){
                              var hides = document.getElementById("resultPosts");
                              if(hides){hides.style.display = 'none';}
                        }
                        else if(item == "resultPosts")
                        {
                              var hides = document.getElementById("resultAlbums");
                              if(hides){hides.style.display = 'none';} 
                        }
                    }
                else{
                    hidePost.style.display = "none";
                    }
            }
        }
    </script>
        <style>
        body{
            font-family: sans-serif, serif;
        }
        #form1{
            display: table; 
            width: 50%;
            margin: auto; 
            padding: 1em;
            background: #EEEEEE;
            border: .1em solid black; 
            border-spacing: 0;
        }
        
        h1{
            color: red;
            padding:0;
            margin: 0;
            text-align: center;
            font-weight: normal;
        }
            
        #result, #resultPosts, #resultAlbums{
            width: 70%;
            margin-left: 15%;
            margin-top: 20px;
            text-align: left;
            border: 1px solid black;
            border-collapse: collapse;
        }
            
        #result th, #resultPosts th, #resultAlbums th{
            border: 1px solid black;
            border-collapse: collapse;
        }
            
        #result td, #resultPosts td, #resultAlbums td{    
            border: 1px solid black;
            border-collapse: collapse;
        }
            
        p{
            display: inline; 
        }
        #location_field{
            <?php
                if(!isset($_POST['type'])){
                    echo "display: none";
                }
                else{
                    if($_POST['type'] == "place"){
                    echo "diplay: table-row";
                    }
                    else{
                        echo "display: none";
                    }
                }
            ?>
        }
        #detailsLink{
            font-size: 1em;
            border:none;
            text-decoration: underline;
            background-color: white;
            cursor: pointer;
            color: blue;
        }
        
        #detailsTitle{
            font-size: 1em;
            width: 70%;
            margin-top:20px;
            margin-left: 15%;
            border:none;
            text-decoration: underline;
            cursor: pointer;
            color: blue;
            background-color: lightgray;
        }
        
        #detailsTitle2{
            font-size: 1em;
            width: 70%;
            margin-left: 15%;
            margin-top: 20px;
            border:none;
            text-decoration: none;
            border: 1px solid black;
            color: black;
            background-color: white;
        }
        
    </style>
    </head>
    <body>
        <main>
            <form action="search.php" method="post" id="form1">
                <h1><i>Facebook Search</i></h1><hr>
                <table>
                    <tr id="keyword_field">
                        <td><p>Keyword</p></td>
                        <td><input id="textAreaKeyword" type="text" <?php if(isset($_POST['type']) || isset($_POST['id'])){echo "value =";echo$_POST['keyword']; }else{}?> name="keyword" required></td>
                    </tr>
                    <tr id="type_field">
                        <td><p>Type:</p></td>
                        <td><select id="selectField" onchange="onTypeSelection()" onload="onTypeSelection()" name="type" >
                        <option value="user" <?php if(!isset($_POST['type'])){echo "selected";}else{if($_POST['type'] == "user"){echo "selected";}}?>>Users</option>
                        <option value="page" <?php if(!isset($_POST['type'])){}else{if($_POST['type'] == "page"){echo "selected";}}?> >Pages</option>
                        <option value="event"<?php if(!isset($_POST['type'])){}else{if($_POST['type'] == "event"){echo "selected";}}?>>Events</option>
                        <option value="place"<?php if(!isset($_POST['type'])){}else{if($_POST['type'] == "place"){echo "selected";}}?>>Places</option>
                        <option value="group"<?php if(!isset($_POST['type'])){}else{if($_POST['type'] == "group"){echo "selected";}}?>>Groups</option>
                        </select></td>
                    </tr>
                    <tr id="location_field" >
                        <td><p>Location:</p></td> 
                        <td> <input id="textAreaLocation" type="text" name="location" <?php 
                                    if(isset($_POST['type'])){echo "value =";if(isset($_POST['location'])){echo $_POST['location'];}else{} }else{echo "value=";}?>></td>       
                        <td><p>Distance(meters)</p></td>
                        <td> <input id="textAreaDistance" pattern="\d{1,15}" title="Only Numbers Allowed" type="text" name="distance" <?php if(isset($_POST['type'])){echo "value =";if(isset($_POST['distance'])){echo $_POST['distance'];}else{}  }else{echo "value=";}?>></td>
                    </tr>
                    <tr>
                        <td><p></p></td>
                        <td><input type="submit" name="Search" value="Search">
                            <input type="button" onclick="emptyFields()" name="Clear" value="Clear"></td>
                    </tr>
                </table>
            </form>
        </main>
    </body>
</html>
    
    <?php
    $keyword;$type;$location;$distance;
    if(isset($_POST['Search']))
    {
        processQuery();
    } 

    if(isset($_POST['id']))
    {
        getDetails();
    }

    function processQuery()
    {
        $keyword = $_POST["keyword"];
        $type = $_POST["type"];
        $location = $_POST["location"];
        $distance = $_POST["distance"];
        $link = 'search?q=';
        
        if($type === "user" || $type === "page" || $type === "group")
        {
            
            $link = $link.$keyword.'&type='.$type.'&fields=id,name,picture.width(700).height(700)&access_token=EAAJdx7RriqkBADg0xPLciJfGlu0kyb25Yg53ZBT6qoWacoCqC43YyBLL21pbu3xkRgvJ55EiaaGrY0Nnyi0azgGenF2Tx3lKMqEOwlQHow7UvtERU297gWiWOb8lqh2xz8skK1yoqZCoMXaapa';
            $fbApp = new Facebook\Facebook(['app_id' => '666062260243113',
                                            'app_secret' => '220f5ff5a7df13e6a4953a8376f17730',
                                            'default_graph_version' => 'v2.8']);
            $response = $fbApp->get($link, 'EAAJdx7RriqkBADg0xPLciJfGlu0kyb25Yg53ZBT6qoWacoCqC43YyBLL21pbu3xkRgvJ55EiaaGrY0Nnyi0azgGenF2Tx3lKMqEOwlQHow7UvtERU297gWiWOb8lqh2xz8skK1yoqZCoMXaapa');
            
            $albumsEdge = $response->getGraphEdge();

            $result = json_decode($albumsEdge,true);
            if(empty($result))
            {
                echo "<table id=result><tr><td style= text-align:center>No Records have been found</tr></td></table>"; 
            }
            else
            {
                echo "<form action=search.php method=post>";
                echo "<table id=result>".
                "<tr>".
                "<th>Profile Photo</th>".
                "<th>Name</th>".
                "<th>Details</th>".
                "</tr>";
                for($i = 0; $i < count($result); $i++)
                {   
                    $url = $result[$i]['picture']['url'];
                    echo "<td><a href=".$url." target=_blank><img src=".$url." width=40px heigh=30px></a></td>";
                    echo "<td>".$result[$i]['name']."</td>"; 
                    echo "<td><button id= detailsLink type=submit name= id value=".$result[$i]['id'].">Details</button></td>";
                    echo "<td style= display:none ><input style= display:none type=text value=".$keyword." name=keyword></td>";
                    echo "<td style= display:none ><input style= display:none type=text value=".$type." name=type></td>";
                    echo "<td style= display:none ><input style= display:none type=text value=".$location." name=location></td>";
                    echo "<td style= display:none ><input style= display:none type=text value=".$distance." name=distance></td>";
                    
                    echo "</tr>";
                }
                echo "</table>";
                echo "</form>";
            }      
        }
        else if($type === "place")
        {
            $linkGeocode =  "https://maps.googleapis.com/maps/api/geocode/json?address=".$location."&key=AIzaSyBOFaac9Lrd7DzoNLFieUCZw7GxYGTZueM";
            $jsonGeocode = @file_get_contents($linkGeocode);
            $resultGeocode =  json_decode($jsonGeocode, true);
            
                if(empty($resultGeocode) && !empty($location))
                {
                     echo "<table id=result><tr><td style= text-align:center>Adress Is Invalid</tr></td></table>"; 
                     return;
                }
                if(empty($location) && !empty($distance))
                {
                     echo "<table id=result><tr><td style= text-align:center>Distance specified without location or address</tr></td></table>"; 
                    return; 
                }
                else
                {     
                    if(empty($location) && empty($distance))
                    {
                         $link = $link.$keyword.'&type='.$type.'&fields=id,name,picture.width(700).height(700)&access_token=EAAJdx7RriqkBADg0xPLciJfGlu0kyb25Yg53ZBT6qoWacoCqC43YyBLL21pbu3xkRgvJ55EiaaGrY0Nnyi0azgGenF2Tx3lKMqEOwlQHow7UvtERU297gWiWOb8lqh2xz8skK1yoqZCoMXaapa';
                    }
                    else{
                        $lat = $resultGeocode['results'][0]['geometry']['location']['lat'];
                        $long = $resultGeocode['results'][0]['geometry']['location']['lng'];
                        
                        $link = $link.$keyword.'&type='.$type.'&center='.$lat.','.$long.'&distance'.$distance.'&fields=id,name,picture.width(700).height(700)&access_token=EAAJdx7RriqkBADg0xPLciJfGlu0kyb25Yg53ZBT6qoWacoCqC43YyBLL21pbu3xkRgvJ55EiaaGrY0Nnyi0azgGenF2Tx3lKMqEOwlQHow7UvtERU297gWiWOb8lqh2xz8skK1yoqZCoMXaapa'; 
                    }
                    
                    $fbApp = new Facebook\Facebook(['app_id' => '666062260243113',
                                            'app_secret' => '220f5ff5a7df13e6a4953a8376f17730',
                                            'default_graph_version' => 'v2.8']);
                    $response = $fbApp->get($link, 'EAAJdx7RriqkBADg0xPLciJfGlu0kyb25Yg53ZBT6qoWacoCqC43YyBLL21pbu3xkRgvJ55EiaaGrY0Nnyi0azgGenF2Tx3lKMqEOwlQHow7UvtERU297gWiWOb8lqh2xz8skK1yoqZCoMXaapa');
            
                    $albumsEdge = $response->getGraphEdge();

                    $result = json_decode($albumsEdge,true);
                    if(empty($result))
                    {
                        echo "<table id=result><tr><td style= text-align:center>No Records have been found</tr></td></table>"; 
                    }
                    else
                    {
                        echo "<form action=search.php method=post>";
                        echo "<table id=result>".
                            "<tr>".
                            "<th>Profile Photo</th>".
                            "<th>Name</th>".
                            "<th>Details</th>".
                            "</tr>";
                        for($i = 0; $i < count($result); $i++)
                        {   
                            $url = $result[$i]['picture']['url'];
                            echo "<td><a href=".$url." target=_blank><img src=".$url." width=40px heigh=30px></a></td>";
                            echo "<td>".$result[$i]['name']."</td>"; 
                            echo "<td><button id= detailsLink type=submit name= id value=".$result[$i]['id'].">Details</button></td>";
                            echo "<td style= display:none ><input style= display:none type=text value=".$keyword." name=keyword></td>";
                            echo "<td style= display:none ><input style= display:none type=text value=".$type." name=type></td>";
                            echo "<td style= display:none ><input style= display:none type=text value=".$location." name=location></td>";
                            echo "<td style= display:none ><input style= display:none type=text value=".$distance." name=distance></td>";
                    
                            echo "</tr>";
                        }
                        echo "</table>";
                        echo "</form>";
                    }      
                }
            }
        else if($type === "event")
        {
             $link = $link.$keyword.'&type='.$type.'&fields=id,name,place,picture.width(700).height(700)&access_token=EAAJdx7RriqkBADg0xPLciJfGlu0kyb25Yg53ZBT6qoWacoCqC43YyBLL21pbu3xkRgvJ55EiaaGrY0Nnyi0azgGenF2Tx3lKMqEOwlQHow7UvtERU297gWiWOb8lqh2xz8skK1yoqZCoMXaapa';
            $fbApp = new Facebook\Facebook(['app_id' => '666062260243113',
                                            'app_secret' => '220f5ff5a7df13e6a4953a8376f17730',
                                            'default_graph_version' => 'v2.8']);
            $response = $fbApp->get($link, 'EAAJdx7RriqkBADg0xPLciJfGlu0kyb25Yg53ZBT6qoWacoCqC43YyBLL21pbu3xkRgvJ55EiaaGrY0Nnyi0azgGenF2Tx3lKMqEOwlQHow7UvtERU297gWiWOb8lqh2xz8skK1yoqZCoMXaapa');
            
            $albumsEdge = $response->getGraphEdge();

            $result = json_decode($albumsEdge,true);
            if(empty($result))
            {
               echo "<table id=result><tr><td style= text-align:center>No ".$type." Has Been Found</tr></td></table>";   
            }
            else
            {
                echo "<table id=result>".
                "<tr>".
                "<th>Profile Photo</th>".
                "<th>Name</th>".
                "<th>Place</th>".
                "</tr>";
                for($i = 0; $i < count($result); $i++)
                {   
                    if(array_key_exists('picture', $result[$i]))
                    {
                        echo "<td><a href=".$result[$i]['picture']['url']." target=_blank><img src=".$result[$i]['picture']['url']." width=30px heigh=30px></a></td>";}else{echo "<td></td>";
                    }
                    if(array_key_exists('name', $result[$i])){echo "<td>".$result[$i]['name']."</td>";}else{echo "<td></td>";}
                    if(array_key_exists('place', $result[$i])){
                        $places = $result[$i]['place'];  
                        if(array_key_exists('name', $places))
                        {
                            echo "<td>".$places['name']."</td>";
                        }
                        else{
                             echo "<td></td>";
                        }
                    }else
                    {
                        echo "<td></td>";
                    }
                    echo "</tr>";
                }
                echo "</table>";
            }      
        }   
    }

    //OBTAIN DETAILS FROM PAGE
    function getDetails()
    {
         echo "<br>";
         echo "<div id =detailsPHP>";
         $id = $_POST['id'];
         $link = $id.'?&fields=id,name,picture.width(700).height(700),albums.limit(5){name,photos.limit(2){name,picture}},posts.limit(5)&access_token=EAAJdx7RriqkBADg0xPLciJfGlu0kyb25Yg53ZBT6qoWacoCqC43YyBLL21pbu3xkRgvJ55EiaaGrY0Nnyi0azgGenF2Tx3lKMqEOwlQHow7UvtERU297gWiWOb8lqh2xz8skK1yoqZCoMXaapa';
            $fbApp = new Facebook\Facebook(['app_id' => '666062260243113',
                                            'app_secret' => '220f5ff5a7df13e6a4953a8376f17730',
                                            'default_graph_version' => 'v2.8']);
            $response = $fbApp->get($link, 'EAAJdx7RriqkBADg0xPLciJfGlu0kyb25Yg53ZBT6qoWacoCqC43YyBLL21pbu3xkRgvJ55EiaaGrY0Nnyi0azgGenF2Tx3lKMqEOwlQHow7UvtERU297gWiWOb8lqh2xz8skK1yoqZCoMXaapa');
            
            $albumsEdge = $response->getGraphNode();

            $result = json_decode($albumsEdge,true);
            if(empty($result))
            {
                    echo "<button id=detailsTitle style:background-color:white>No Posts Has Been Found</button >";
            }
            else
            {
                if(!array_key_exists('albums', $result))
                {
                    echo "<button id=detailsTitle2>No Albums Has Been Found</button>";
                }
                else
                {   
                    $albums = $result['albums'];
                    echo "<button id=detailsTitle onclick=hide('resultAlbums') >Albums</button>";
                    echo "<table id=resultAlbums style= display:none>";
                    for($i = 0; $i < count($albums); $i++)
                    {   
                        if(array_key_exists('photos', $albums[$i]))
                        {
                            $photos = $albums[$i]['photos'];
                            echo "<tr>";
                            if(array_key_exists('name', $albums[$i]))
                            {
                                if(count($photos)>0)
                                {
                                    echo "<td><button id=detailsLink onclick=hide('albumPhoto".$i."') >".$albums[$i]['name']."</button><br? <br>";
                                }
                                else
                                {
                                    echo "<td><button disabled style= border:none background-color:white >".$albums[$i]['name']."</button><br? <br>";
                                }
                            }
                            echo "<p id = albumPhoto".$i." style = display:none>";
                            for($j = 0; $j < count($photos); $j++)
                            {   
                                if(array_key_exists('picture', $photos[$j]))
                                {
                                     $link = $photos[$j]['id'].'/picture?access_token=EAAJdx7RriqkBADg0xPLciJfGlu0kyb25Yg53ZBT6qoWacoCqC43YyBLL21pbu3xkRgvJ55EiaaGrY0Nnyi0azgGenF2Tx3lKMqEOwlQHow7UvtERU297gWiWOb8lqh2xz8skK1yoqZCoMXaapa&redirect=false';
     
                                     $response2 = $fbApp->get($link, 'EAAJdx7RriqkBADg0xPLciJfGlu0kyb25Yg53ZBT6qoWacoCqC43YyBLL21pbu3xkRgvJ55EiaaGrY0Nnyi0azgGenF2Tx3lKMqEOwlQHow7UvtERU297gWiWOb8lqh2xz8skK1yoqZCoMXaapa');
                                
                                    $originalImage = $response2->getGraphNode();
                                    $result2 = json_decode($originalImage,true);

                                    echo "<a href=".$result2['url']." target=_blank><img src =". $photos[$j]['picture']." width = 80px height = 80px></a>&nbsp";
                                }
                            }
                            echo "</td>";
                            echo "</tr>";
                        }
                    }
                    echo "</table>";
                }
                
                if(!array_key_exists('posts', $result)){
                    echo "<button id=detailsTitle2>No Posts Has Been Found</button>";
                }
                else{   
                    $posts = $result['posts'];
                    echo "<button id=detailsTitle onclick=hide('resultPosts') >Posts</button>";
                    echo "<table id=resultPosts style= display:none>".
                    "<tr>".
                    "<th>Message</th>".
                    "</tr>";
                    for($i = 0; $i < count($posts); $i++)
                    {   
                        if(array_key_exists('message', $posts[$i]))
                        {
                            echo "<tr>";
                            echo "<td>".$posts[$i]['message']."</td>";
                            echo "</tr>";
                        }
                    }
                    echo "</table>";
                }
            }  
        echo "</div>";
    }
    ?>